﻿using System;
using System.Net;
using System.Threading;
using Owin;

namespace NanoServerMiniService
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            const string port = "3000";
            // notice the "*" to bind to _any_ interface since it's unpredictable in a docker container
            var baseAddress = $"http://*:{port}/";

            using (Microsoft.Owin.Hosting.WebApp.Start<Startup>(baseAddress))
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                Console.WriteLine("Endpoints:");
                foreach (var ip in host.AddressList)
                    Console.WriteLine($"  http://{ip}:{port}");

                Console.WriteLine("Press CTRL+C to stop the service...");
                
                Thread.Sleep(Timeout.Infinite);
            }
        }

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                app.Run(sample =>
                {
                    sample.Response.ContentType = "text/plain";

                    return sample.Response.WriteAsync("Hello from OWIN");
                });
            }
        }
    }
}