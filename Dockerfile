FROM microsoft/dotnet-framework:4.6.2

WORKDIR \app
COPY bin/Debug .

EXPOSE 3000

ENTRYPOINT ["cmd.exe", "/k", "NanoServerMiniService.exe"]